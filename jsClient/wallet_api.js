function WalletAPI(host, key, secret){
    this.host = host;
}

WalletAPI.prototype.setAuth = function(key, secret){
    this.secret = secret;
    this.key = key;
}

WalletAPI.prototype.formatQuery = function(data){
    var query = "";
    for(var key in data){
        query += encodeURIComponent(key) + "=" + encodeURIComponent(data[key]) + "&";
    }
    if(query) query = query.substring(0, query.length-1);
    return query;
}

WalletAPI.prototype.request = function(method, uri, data, callback){
    var request = new XMLHttpRequest();
    if(data === null && typeof data !== 'object') data = {}

    request.onreadystatechange=function(){
        if (request.readyState==4){
            if(request.status==200){
                callback(null, request.responseText.length?JSON.parse(request.responseText):undefined);
            }else{
                callback(request.status);
            }
        }
    }

    var query;
    request.open(method, "https://"+this.host+"/"+uri, true);
    if(this.key&&this.secret){
        data["key"] = this.key;
        data["nonce"] = Date.now()/1000;
        query = this.formatQuery(data);
        request.setRequestHeader("signature", CryptoJS.HmacSHA1(query, this.secret));
    }else{
        query = this.formatQuery(data);
    }
    if(query){
        request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        request.send(query);
    }else{
        request.send();
    }
}


WalletAPI.prototype.post = function(uri, data, callback){
    this.request("POST", uri, data, callback);
}

WalletAPI.prototype.get = function(uri, callback){
    this.request("GET", uri, {}, callback);
}

WalletAPI.prototype.delete = function(uri, data, callback){
    this.request("DELETE", uri, data, callback);
}


//PUBLIC API's

WalletAPI.prototype.price = function(callback){
    this.get("price", callback)
}

WalletAPI.prototype.checkout = function(paymentID, text, callback){
    this.post("checkout", {payment_id:paymentID, text:text}, callback);
}

WalletAPI.prototype.checkPayment = function(checkoutID, paymentID, callback){
    this.post("check", {id:checkoutID, payment_id:paymentID}, callback);
}

//PRIVATE API's

WalletAPI.prototype.balance = function(callback){
    this.post("api/balance",{}, callback);
}

WalletAPI.prototype.send = function(amount, address, callback){
    this.post("api/send", {amount:amount, address:address}, callback);
}

WalletAPI.prototype.transactions = function(callback){
    this.post("api/transactions", {}, callback);
}

WalletAPI.prototype.addresses = function(callback){
    this.post("api/addresses",{}, callback);
}

WalletAPI.prototype.generateAddress = function(callback){
    this.post("api/generate",{}, callback);
}

WalletAPI.prototype.paymentTemplates = function(callback){
    this.post("api/payment-templates",{}, callback);
}

WalletAPI.prototype.addPaymentTemplate = function(amount, alias, checkoutCallback, callback){
    this.post("api/payment-template/add", {amount:amount, alias:alias, callback:checkoutCallback}, callback);
}

WalletAPI.prototype.removePaymentTemplate = function(id, callback){
    this.post("api/payment-template/delete", {id: id}, callback);
}

WalletAPI.prototype.invoices = function(payment_id, callback){
    this.post("api/invoices", {payment_id:payment_id}, callback);
}

WalletAPI.prototype.activate2FA = function(callback){
    this.post("api/2fa/activate", {}, callback);
}

WalletAPI.prototype.generate2FA = function(callback){
    this.post("api/2fa/generate", {}, callback);
}

WalletAPI.prototype.generateAPIKey = function(callback){
    this.post("api/key/generate", {}, callback);
}

WalletAPI.prototype.removeAPIKey = function(id, callback){
    this.post("api/key/cancel", {id:id}, callback);
}




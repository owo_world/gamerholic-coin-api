https = require 'https'
crypto = require 'crypto'

class APIClient
  constructor:(@host, @port, @key, @secret)->

  setAuth:(@key, @secret)->

  get: (uri, callback)->
    @request "GET", uri, {}, callback

  post: (uri, data, callback)->
    @request "POST", uri, data, callback

  delete: (uri, data, callback)->
    @request "DELETE", uri, {}, callback

  request: (method, uri, data, callback)->
    data.nonce = Date.now()
    data.key = @key

    vars = []
    for key,val of data
      vars.push "#{key}=#{val}"
    queryString = vars.join "&"

    options =
      host: @host
      port: @port
      method: method
      path: uri
      headers:
        Host: @host
        'Content-Length': queryString.length

    if @secret? then options.headers.signature = @sign queryString
    options.rejectUnauthorized = false


    request = https.request options
    request.on "error", (err)->
      callback err
    request.on "response", (response)->
      buffer = ''
      response.on 'data', (chunk)->
        buffer += chunk
      response.on 'end', ()->
        if response.statusCode == 200
          callback null, buffer
        else
          callback buffer, null
    request.end queryString

  sign:(text)->
    hmac = crypto.createHmac "sha1", @secret
    return hmac.update(text).digest("hex") 

module.exports = APIClient

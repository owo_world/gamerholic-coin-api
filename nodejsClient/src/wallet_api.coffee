APIClient = require "./api_client"

class WalletAPI extends APIClient
  price:(callback)->
    @get "/price", callback

  register:(email, callback)->
    @post "/api/register", {email:email}, callback

  checkout:(payment_id, text, callback)->
    @post "/checkout", {payment_id:payment_id, text:text}, callback

  check:(id, payment_id, callback)->
    @post "/check", {id:id, payment_id:payment_id}, callback

  balance:(callback)->
    @post "/api/balance", {}, callback

  send:(amount, address, callback)->
    @post "/api/send", {amount:amount, address:address}, callback

  transactions:(callback)->
    @post "/api/transactions", {}, callback

  addresses:(callback)->
    @post "/api/addresses", {}, callback

  generateAddress:(callback)->
    @post "/api/generate", {}, callback

  paymentTemplates:(callback)->
    @post "/api/payment-templates", {}, callback

  addPaymentTemplate:(amount, alias, checkoutCallback, callback)->
    @post "/api/payment-template/add", {amount:amount, alias:alias, callback:checkoutCallback}, callback

  removePaymentTemplate:(id, callback)->
    @post "/api/payment-template/delete", {id: id}, callback

  invoices:(payment_id, callback)->
    @post "/api/invoices", {payment_id:payment_id}, callback

  activate2FA:(callback)->
    @post "/api/2fa/activate", {}, callback

  generate2FA:(callback)->
    @post "/api/2fa/generate", {}, callback

  generateAPIKey:(callback)->
    @post "/api/key/generate", {}, callback

  removeAPIKey:(id, callback)->
    @post "/api/key/cancel", {id:id}, callback

module.exports = WalletAPI

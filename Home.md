A simple Object Oriented wrapper for Wallet API, written with PHP.

#Requirements
PHP >= 5.3.2 with cURL extension,


#Usage

```
#!php

require_once 'wallet_api.php';

$wallet = new WalletAPI(array("host"=>"gamerholic.io:3001"));

....

//to authenticate, use
$wallet->setAuth($key, $secret);

...
```

A complete list of [API's](API.md) can be found [here](API.md)
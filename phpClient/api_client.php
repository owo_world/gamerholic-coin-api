<?php


class APIClient
{
    public function __construct($options=array())
    {
        $this->options = $options;
    }
    
    public function setAuth($key, $secret)
    {
        $this->options["key"] = $key;
        $this->options["secret"] = $secret;
    }

    public function get($uri, $parameters=array())
    {
        return $this->execute($uri, 'GET', $parameters);
    }
    
    public function post($uri, $parameters=array())
    {
        return $this->execute($uri, 'POST', $parameters);
    }
    
    public function put($uri, $parameters=array() )
    {
        return $this->execute($uri, 'PUT', $parameters);
    }
    
    public function delete($uri, $parameters=array())
    {
        return $this->execute($uri, 'DELETE', $parameters);
    }


    public function execute($uri, $method='GET', $parameters=array())
    {
        $headers = array();
        if(isset($this->options["secret"]))
        {
            $parameters['key'] = $this->options["key"];
            $parameters['nonce'] = round(microtime(true) * 1000);
            $query = $this->formatQuery($parameters);
            $headers[] = 'signature: '.$this->sign($query);
        }
        else
        {
            $query = $this->formatQuery($parameters);
        }

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, "https://".$this->options["host"]."/".$uri);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt ($handle, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($handle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($handle, CURLOPT_HEADER, FALSE);
        if($query||$method=="POST")
        {
            curl_setopt($handle, CURLOPT_POST, TRUE);
            curl_setopt($handle, CURLOPT_POSTFIELDS, $query);
        }
        curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($handle);
        curl_close($handle);
        $obj = json_decode($response);
        return $obj?$obj:$response;
    }

    public function sign($text)
    {
        return hash_hmac('sha1', $text, $this->options["secret"]);
    }
    
    public function formatQuery($parameters)
    {
        $vars = array();
        foreach($parameters as $key => $value)
        {
            $vars[] = $key."=".$value;
        }
        return implode("&", $vars);
    }

}



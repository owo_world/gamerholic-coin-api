<?php

require_once "api_client.php";


class WalletAPI extends APIClient
{
    public function price()
    {
        return $this->get("price");
    }
    public function register($email)
    {
        return $this->post("api/register", $email?array("email"=>$email):array());
    }
    public function checkout($payment_id, $text)
    {
        return $this->post("checkout", array("payment_id"=>$payment_id, "text"=>$text));
    }
    public function check($id, $payment_id)
    {
        return $this->post("check", array("id"=>$id, "payment_id"=>$payment_id));
    }

    public function balance()
    {
        return $this->post("api/balance");
    }
    public function send($amount, $address)
    {
        return $this->post("api/send", array("amount"=>$amount, "address"=>$address));
    }
    public function transactions()
    {
        return $this->post("api/transactions");
    }

    public function addresses()
    {
        return $this->post("api/addresses");
    }

    public function generateAddress()
    {
        return $this->post("api/generate");
    }

    public function paymentTemplates()
    {
        return $this->post("api/payment-templates");
    }

    public function addPaymentTemplate($amount, $title, $checkoutCallback, $callbackKey)
    {
        return $this->post("api/payment-template/add", array("amount"=>$amount, "title"=>$title, "callback"=>$checkoutCallback, "callback_key"=>$callbackKey));
    }

    public function removePaymentTemplate($id)
    {
        return $this->post("api/payment-template/delete", array("id"=>$id));
    }

    public function invoices($payment_id)
    {
        return $this->post("api/invoices", array("payment_id"=>$payment_id));
    }

    public function activate2FA()
    {
        return $this->post("api/2fa/activate");
    }

    public function generate2FA()
    {
        return $this->post("api/2fa/generate");
    }

    public function generateAPIKey()
    {
        return $this->post("api/key/generate");
    }

    public function removeAPIKey($id)
    {
        return $this->post("api/key/cancel", array("id"=>$id));
    }

}
